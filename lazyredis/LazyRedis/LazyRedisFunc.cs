using System;
using static System.Environment;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using StackExchange.Redis.Extensions.AspNetCore; // AddStackExchangeRedisExtensions
using StackExchange.Redis.Extensions.Core; // ISerializer
using StackExchange.Redis.Extensions.Core.Abstractions; // IRedisDatabase
using StackExchange.Redis.Extensions.Core.Configuration; // configuration
using StackExchange.Redis.Extensions.Core.Implementations; // RedisClientFactory
using StackExchange.Redis.Extensions.Newtonsoft; //serialization

namespace LazyRedis {
    public static class LazyRedisFunc {
        // private readonly IRedisClient _redisClient;
        private static Lazy<IRedisClient> lazyClient = new Lazy<IRedisClient>(InitializeRedisClient());
        private static IRedisClient _redisClient => lazyClient.Value;

        private static IRedisClient InitializeRedisClient() {
            // Perform any initialization here
            try {
                RedisConfiguration redisConfiguration = new RedisConfiguration();
                redisConfiguration.ConnectionString = GetEnvironmentVariable("Redis:ConnectionString");
                redisConfiguration.PoolSize = int.Parse(GetEnvironmentVariable("Redis:PoolSize"));
                redisConfiguration.AllowAdmin = bool.Parse(GetEnvironmentVariable("Redis:AllowAdmin"));
                redisConfiguration.Name = GetEnvironmentVariable("Redis:Name");
                ISerializer serializer = new NewtonsoftSerializer();
                var redisClientFactory = new RedisClientFactory(new [] { redisConfiguration }, default, serializer);
                var redisClient = redisClientFactory.GetRedisClient();
                return redisClient;

            } catch {
                throw;
            } finally {

            }
        }

        [FunctionName("LazyRedisFunc")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log) {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            var cachedValue = await _redisClient.GetDefaultDatabase().GetAsync<string>(name.ToLower());
            if (String.IsNullOrEmpty(cachedValue)) {
                bool isAdded = await _redisClient.GetDefaultDatabase().AddAsync<string>(name.ToLower(), name, DateTimeOffset.Now.AddMinutes(2));
                log.LogInformation($"name: {name} is cached {isAdded}");

                string responseMessage = string.IsNullOrEmpty(name) ?
                    "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response." :
                    $"Hello, {name}. You have been added to cache successfully.";

                return new OkObjectResult(responseMessage);
            } else {
                log.LogInformation($"name: {name} is found in cache {cachedValue}");
                string responseMessage = string.IsNullOrEmpty(name) ?
                    "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response." :
                    $"Hi again, {cachedValue}. You have been successfully found in cache.";

                return new OkObjectResult(responseMessage);
            }
        }
    }
}