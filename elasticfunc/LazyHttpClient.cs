using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace elasticfunc {
    public static class LazyHttpClient {
        /*
        A common question about HttpClient in .NET is "Should I dispose of my client?" 
        In general, you dispose of objects that implement IDisposable 
        when you're done using them. But you don't dispose of a static client 
        because you aren't done using it when the function ends. 
        You want the static client to live for the duration of your application.
        */
        private static HttpClient httpClient = new HttpClient();

        [FunctionName("LazyHttpClient")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log) {
            log.LogInformation("C# HTTP trigger function processed a request.");

            var response = await httpClient.GetAsync("https://mocktarget.apigee.net/echo");
            string requestBody = await response.Content.ReadAsStringAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            return new OkObjectResult(data);
        }
    }
}