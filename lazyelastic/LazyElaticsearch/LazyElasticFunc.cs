using System;
using System.Collections.Specialized;
using System.IO;
using System.Threading.Tasks;
using Elasticsearch.Net;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Nest;
using Newtonsoft.Json;
using Xunit;
using static System.Environment;

namespace LazyElaticsearch {
    public static class LazyElasticFunc {
        private static Lazy<IElasticClient> lazyClient = new Lazy<IElasticClient>(InitializeElasticClient);
        private static IElasticClient elasticClient => lazyClient.Value;

        private static IElasticClient InitializeElasticClient() {
            // Perform any initialization here
            try {
                var uri = new Uri(GetEnvironmentVariable("Elasticsearch:Endpoint"));
                // var pool = new StaticConnectionPool(new Uri[] { uri });
                var settings = new ConnectionSettings(uri);
                return new ElasticClient(settings);

            } catch {
                throw;
            } finally {

            }
        }

        [FunctionName("LazyElasticFunc")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log) {
            // log.LogInformation("C# HTTP trigger function processed a request.");

            string name = req.Query["name"];

            var user_token = elasticClient
                .Security.GetUserAccessToken(
                    GetEnvironmentVariable("Elasticsearch:BasicAuthUser"),
                    GetEnvironmentVariable("Elasticsearch:BasicAuthPassword"),
                    c => c
                    .RequestConfiguration(r => r
                        .BasicAuthentication(
                            GetEnvironmentVariable("Elasticsearch:BasicAuthUser"),
                            GetEnvironmentVariable("Elasticsearch:BasicAuthPassword")
                        )
                    )
                );

            Assert.True(user_token.IsValid, "User_Token failed.");
            log.LogInformation($"GetUserAccessToken IsValid: {user_token.IsValid}");
            log.LogInformation($"AccessToken: {user_token.AccessToken}");

            var result = elasticClient.PingAsync(
                c => c
                .RequestConfiguration(
                    r => r.GlobalHeaders(
                        new NameValueCollection { { "Authorization", $"Bearer {user_token.AccessToken}" } }
                    )
                )
            ).GetAwaiter().GetResult();
            Assert.True(result.IsValid, "Ping failed.");

            log.LogInformation($"Ping IsValid: {result.IsValid}");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            name = name ?? data?.name;

            // string responseMessage = string.IsNullOrEmpty(name) ?
            //    "This HTTP triggered function executed successfully. Pass a name in the query string or in the request body for a personalized response." :
            //    $"Hello, {name}. This HTTP triggered function executed successfully.";

            return new OkObjectResult(new { AccessToken = user_token.AccessToken });
        }

        [FunctionName("GetElasticAccessToken")]
        public static async Task<IActionResult> GetElasticAccessToken(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "ElasticAccessToken")] HttpRequest req,
            ILogger log) {
            GetUserAccessTokenResponse user_token = await elasticClient
                .Security.GetUserAccessTokenAsync(
                    GetEnvironmentVariable("Elasticsearch:BasicAuthUser"),
                    GetEnvironmentVariable("Elasticsearch:BasicAuthPassword"),
                    c => c
                    .RequestConfiguration(r => r
                        .BasicAuthentication(
                            GetEnvironmentVariable("Elasticsearch:BasicAuthUser"),
                            GetEnvironmentVariable("Elasticsearch:BasicAuthPassword")
                        )
                    )
                );

            var result = user_token
            switch {
                var t when t.IsValid => new OkObjectResult(new {
                t.AccessToken,
                t.ExpiresIn,
                t.Scope,
                t.Type
                }),
                _ =>
                new ObjectResult(new { StatusCode = 404, user_token.IsValid, user_token.DebugInformation }),

            };
            return result;

        }
    }
}