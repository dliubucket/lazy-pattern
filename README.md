# Http Trigger Function

```
func init elasticfunc --dotnet
cd elasticfunc
func new --name LazyHttpClient --template "HTTP trigger" --authlevel "anonymous"
func start

curl -X GET "http://localhost:7071/api/LazyHttpClient?name=David"
```

# CosmosDB

```
func init lazycosmosdb --dotnet
cd lazycosmosdb
lazycosmosdb$ func new --name LazyCosmosClient --template "HTTP trigger" --authlevel "anonymous"
dotnet add package Microsoft.Azure.Cosmos

func start
```

# Checking TIME_AWAIT items

```
sysctl net.inet.tcp.tw_pcbcount
```

# Lazy Redis Functions

```
dotnet new sln -n lazyredis -o lazyredis
cd lazyredis
func init LazyRedis --dotnet
cd LazyRedis
func new --name LazyRedisFunc --template "HTTP trigger" --authlevel "anonymous"
```


```
dotnet add package StackExchange.Redis.Extensions.Core --version 8.0.3
dotnet add package StackExchange.Redis.Extensions.AspNetCore --version 8.0.3
dotnet add package StackExchange.Redis.Extensions.Newtonsoft --version 8.0.3
```

```
curl -X GET "http://localhost:7071/api/LazyRedisFunc?name=David"
```

# Lazy Elasticsearch Function

```
dotnet new sln -n lazyelastic -o lazyelastic
func init LazyElaticsearch --dotnet
cd LazyElaticsearch 
LazyElaticsearch$ func new --name LazyElasticFunc --template "HTTP trigger" --authlevel "anonymous"
```

### GetUserAccessToken
```
curl -X GET "http://localhost:7071/api/ElasticAccessToken"
```