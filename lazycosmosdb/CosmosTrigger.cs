using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using lazycosmosdb.Models;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Documents;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static System.Environment;

namespace lazycosmosdb {
    public static class CosmosTrigger {
        private static Lazy<CosmosClient> lazyClient = new Lazy<CosmosClient>(InitializeCosmosClient);
        private static CosmosClient cosmosClient => lazyClient.Value;

        private static CosmosClient InitializeCosmosClient() {
            // Perform any initialization here
            try {
                return new CosmosClient(GetEnvironmentVariable("CosmosDB:Url"), GetEnvironmentVariable("CosmosDB:Key"));

            } catch {
                throw;
            } finally {

            }
        }

        public static string CosmosDatabase = System.Environment.GetEnvironmentVariable("CosmosDB:Database");
        public static string ConnectionString = System.Environment.GetEnvironmentVariable("CosmosDB:ConnectionString");

        [FunctionName("CosmosTrigger")]
        public static async Task Run(
            [CosmosDBTrigger(
                databaseName: "%CosmosDB:Database%",
                collectionName: "%CosmosDB:Container%",
                ConnectionStringSetting = "CosmosDBConnection",
                LeaseCollectionName = "%CosmosDB:Leases%", // The lease collection, if partitioned, must have partition key equal to id.
                CreateLeaseCollectionIfNotExists = true
            )] IReadOnlyList<Document> documents,

            ILogger log) {
            if (documents != null && documents.Count > 0) {
                log.LogInformation("Documents modified " + documents.Count);
                log.LogInformation("First document Id " + documents[0].Id);

                foreach (Document document in documents) {
                    try {
                        Container container = cosmosClient.GetContainer(GetEnvironmentVariable("CosmosDB:Database"), GetEnvironmentVariable("CosmosDB:Container"));
                        WeatherForecast readResponse =
                            await container.ReadItemAsync<WeatherForecast>(
                                document.Id,
                                new Microsoft.Azure.Cosmos.PartitionKey(document.Id)
                            );
                        log.LogInformation(readResponse.ToString());

                    } catch (Exception ex) {
                        log.LogError(ex, "Failed to process document");
                    }
                }
            }
        }
    }
}