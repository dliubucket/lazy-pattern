using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using lazycosmosdb.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static System.Environment;

namespace lazycosmosdb {

    public static class LazyCosmosClient {

        private static Lazy<CosmosClient> lazyClient = new Lazy<CosmosClient>(InitializeCosmosClient);
        private static CosmosClient cosmosClient => lazyClient.Value;

        private static CosmosClient InitializeCosmosClient() {
            // Perform any initialization here
            try {
                return new CosmosClient(GetEnvironmentVariable("CosmosDB:Url"), GetEnvironmentVariable("CosmosDB:Key"));

            } catch {
                throw;
            } finally {

            }
        }

        [FunctionName("LazyCosmosClient")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", "post", Route = null)] HttpRequest req,
            ILogger log) {
            /*
            Database database = await cosmosClient.CreateDatabaseIfNotExistsAsync("lazydatabase");
            Container container = await database.CreateContainerIfNotExistsAsync(
                "lazycollection",
                "/myPartitionKey",
                400);
            */

            Container container = cosmosClient.GetContainer(GetEnvironmentVariable("CosmosDB:Database"), GetEnvironmentVariable("CosmosDB:Container"));

            var rng = new Random();
            var forecast = new WeatherForecast {
                id = Guid.NewGuid().ToString(),
                date = DateTime.Now.AddDays(rng.Next(0, Summary.Summaries.Length)),
                temperatureC = rng.Next(-20, 55),
                summary = Summary.Summaries[rng.Next(Summary.Summaries.Length)]
            };
            forecast.partitionKeyPath = forecast.id;

            WeatherForecast createdResponse = await container.UpsertItemAsync<WeatherForecast>(forecast, new PartitionKey(forecast.partitionKeyPath));

            log.LogInformation($"Upserted document: {createdResponse.id}");
            return new OkObjectResult(createdResponse);
        }
    }
}