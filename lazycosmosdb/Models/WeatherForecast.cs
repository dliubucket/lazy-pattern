using System;
using Newtonsoft.Json;

namespace lazycosmosdb.Models {
    static public class Summary {

        public static readonly string[] Summaries = new [] {
            "Freezing",
            "Bracing",
            "Chilly",
            "Cool",
            "Mild",
            "Warm",
            "Balmy",
            "Hot",
            "Sweltering",
            "Scorching"
        };
    }
    public class WeatherForecast {
        public string id { get; set; }

        public DateTime date { get; set; }

        public int temperatureC { get; set; }

        public int temperatureF => 32 + (int)(temperatureC / 0.5556);

        public string summary { get; set; }

        [JsonProperty(PropertyName = "myPartitionKey")]
        public string partitionKeyPath { get; set; }

        public override string ToString() {
            return $"Id: {id}, Temperature(°C): {temperatureC}, Temperature(°F): {temperatureF}, Summary: {summary}";

        }
    }
}